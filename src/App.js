import React, { Component, Fragment } from 'react';
import Header from './components/Header';
import Footer from './components/Footer';
import Materias from './components/Materias';

class App extends Component {
	render() {
		const year = new Date().getFullYear();
		const materias = [
			{ nombre: 'BD', nota: 4 },
			{ nombre: 'Calculo', nota: 2 },
			{ nombre: 'Ces4', nota: 3 },
			{ nombre: 'Matematicas', nota: 3 }
		];
		return (
			/** Con JSX */
			<Fragment>
				<Header titulo="My APP React" nombre="Materias" color="green" fontSize="20px" />
				<Materias materias={materias} />
				<Footer nombre="Giovanny" year={year} />
			</Fragment>
		);
		// /**Sin JSX */
		// React.createElement('div',{id:"container"},
		// React.createElement('h1',{id:"letras"},"Hola")));
	}
}

export default App;
