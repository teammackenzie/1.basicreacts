import React, { Component } from 'react';

export default class Header extends Component {
	render() {
		const { titulo, nombre, color, fontSize } = this.props;
		const style = { color, fontSize };
		return (
			<header style={style}>
				<h3>{`${titulo} ${nombre}`}</h3>
			</header>
		);
	}
}
