import React from 'react';
export default function Footer({ nombre, year }) {
	return (
		<footer>
			By {nombre} &copy;{year}
		</footer>
	);
}
