import React, { Component } from 'react';
import Materia from './Materia';

export default class Materias extends Component {
	render() {
		const { materias } = this.props;
		return (
			<div>
				<h1>Estado Materias:</h1>
				<ul>{materias.map((materia, i) => <Materia key={i} materia={materia} />)}</ul>
			</div>
		);
	}
}
