import React from 'react';

export default function Materia(props) {
	const { nombre, nota } = props.materia;
	return (
		<React.Fragment>
			<li>{`Materia: ${nombre} - Nota:${nota}`}</li>
		</React.Fragment>
	);
}
